/* global $ window mixitup document */
$(function () {
    'use strict';
    // Adjust Slider Height
    var winH    = $(window).height(),
        navH    = $('.navbar').innerHeight();
    $('.slider, .carousel-item').height(winH - navH);
    // Adjust Home Height
    var winH    = $(window).height(),
        navH    = $('.navbar').innerHeight();
    $('.home').height(winH - navH);
    //mixitup
    mixitup($('.suffle-image'));
     //scroll top
    var scrollTop = $('.scroll-to-top');
    if ($(window).scrollTop() >= 500) {
        if (scrollTop.is(':hidden')) {
            scrollTop.fadeIn(400);
        }
    } else {
        scrollTop.fadeOut(400);
    }
    $('.scroll-to-top').click(function (e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: 0
        }, 1000);
    });
    
});